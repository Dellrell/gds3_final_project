﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateStick : MonoBehaviour {

    private Vector3 stickRot;
    private Vector3 openRot = new Vector3(0, 0, -3.894f);

    // Update is called once per frame
    void Update () {

        stickRot = transform.eulerAngles;
        if (PuzzleTracker.Instance.starsSolved && !PuzzleTracker.Instance.lookingThroughTelescope) {
            StartCoroutine(LerpStick());
        }
    }
    IEnumerator LerpStick() {
        yield return new WaitForSeconds(2);

        while (stickRot.z < openRot.z) {
            Quaternion targetRot = Quaternion.Euler(openRot);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, 0.05f * Time.deltaTime);
            yield return null;
        }
    }
}
