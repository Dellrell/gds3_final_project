﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameSlide : MonoBehaviour {

    void OnMouseDrag() {
        if (InspectionMode.inInspectionMode) {
            //Debug.Log("sliding");
            InspectionMode.SlideFrame(transform);
        }
    }
}
