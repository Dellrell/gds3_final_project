﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TickMetronome : MonoBehaviour {

    private AudioSource tick;

    void Start() {
        tick = GetComponent<AudioSource>();
    }

	// Update is called once per frame
	void Update () {

	    transform.localRotation = Quaternion.Euler((15 * Mathf.Sin(Time.time)), 0, 0);


	    if (Mathf.DeltaAngle(0, transform.localRotation.x) == 14 ||
	        Mathf.DeltaAngle(0, transform.localRotation.x) == -14) {
            tick.Play();
	    }
	}
}
