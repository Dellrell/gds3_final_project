﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    public static bool gameIsPaused;
    public GameObject pauseMenu;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	    if (Input.GetKeyDown(KeyCode.Escape) && pauseMenu != null) {
	        if (!gameIsPaused) {
                PauseGame();
	        }
	        else {
                Resume();
	        }
	    }
	}

    public void PauseGame() {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
        gameIsPaused = true;
    }

    public void Resume() {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
        gameIsPaused = false;
    }

    public void QuitGame() {
        Debug.Log("quit button pressed");
        Application.Quit();
    }

    public void ResetGame() {
        pauseMenu.SetActive(false);
        gameIsPaused = false;
        Time.timeScale = 1;
        Scene activeScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(activeScene.name);
    }

    public void StartGame() {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }
}
