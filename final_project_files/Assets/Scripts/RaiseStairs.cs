﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaiseStairs : MonoBehaviour {
    private float[] targetHeights = new float[] {-10.33f, -8.33f, -6.33f, -4.33f, -2.33f, 0.2399f };
    private float currentTargetHeight;

	// Update is called once per frame
	void Update () {
        int index = PuzzleTracker.Instance.puzzlesCompleted;
        currentTargetHeight = targetHeights[index];
        if (transform.position.y < currentTargetHeight) {
            StartCoroutine(LerpStairHeight(currentTargetHeight));
        }
	}

    IEnumerator LerpStairHeight(float targetHeight) {
        while(transform.position.y < targetHeight) {
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, targetHeight, transform.position.z), 0.05f * Time.deltaTime);
            yield return null;
        }
    }
}
