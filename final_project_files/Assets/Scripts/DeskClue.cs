﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeskClue : MonoBehaviour {
    
    public GameObject drawer;
    public GameObject drawerContents;

    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Sextant") {
            Rigidbody sextantRb = collision.gameObject.GetComponent<Rigidbody>();
            PickupableObject sextantPu = collision.gameObject.GetComponent<PickupableObject>();
            Destroy(sextantPu);
            Destroy(sextantRb);
            collision.gameObject.layer = 0;
            collision.gameObject.transform.SetParent(this.transform);
            collision.transform.localPosition = Vector3.zero;
            collision.transform.localRotation = Quaternion.identity;

            drawer.gameObject.layer = 0;
            Vector3 drawerPos = drawer.transform.localPosition;
            Vector3 target = new Vector3(drawerPos.x, drawerPos.y, drawerPos.z - 1);
            StartCoroutine(openDrawer(target));
            drawerContents.layer = 8;
        }
    }

    IEnumerator openDrawer(Vector3 targetPos) {
        while (drawer.transform.localPosition.z > targetPos.z) {
            drawer.transform.localPosition = Vector3.Lerp(drawer.transform.localPosition, targetPos, 0.8f * Time.deltaTime);
            yield return null;
        }
    }
}
