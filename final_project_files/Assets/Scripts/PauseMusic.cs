﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMusic : MonoBehaviour {
    private RaycastHit rayHit;
    private bool hitStereo;
    private AudioSource backgroundMusic;
    private GameObject stereoLight;
    
	
	// Update is called once per frame
	void Update () {
	    hitStereo = Physics.Raycast(transform.position, transform.forward, out rayHit, 2, 1 << 11) &&
	                rayHit.transform.gameObject.tag == "Stereo";
	    if (hitStereo && Input.GetButtonUp("Fire1")) {
	        backgroundMusic = rayHit.transform.gameObject.GetComponent<AudioSource>();
	        stereoLight = rayHit.transform.Find("Point light").gameObject;
	        if (backgroundMusic.isPlaying) {
                backgroundMusic.Pause();
                stereoLight.SetActive(false);
	        }
	        else {
                backgroundMusic.Play();
                stereoLight.SetActive(true);
	        }
	    }
	}
}
