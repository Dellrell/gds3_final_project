﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//make a new kind of object i guess
[CreateAssetMenu(menuName ="Star Display", fileName ="starDisplay")]
public class StarDisplay : ScriptableObject {

    public Sprite[] sprites;
    public bool inCutScene;

    public Sprite GetCurrentSprite (int currentPuzzle) {
        if (sprites == null || sprites.Length <= 0 || currentPuzzle < 0)
            return null;

        return sprites[currentPuzzle];
    }

    public IEnumerator FadeInStars (Image activeSprite) {
        while (activeSprite.color.a < 1) {
            activeSprite.color = Color.Lerp(new Color(1, 1, 1, 0), new Color(1, 1, 1, 1), 0.05f);
            inCutScene = true;
            yield return null;
        }
        inCutScene = false;
    }
}
