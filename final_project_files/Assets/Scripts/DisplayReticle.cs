﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayReticle : MonoBehaviour {

    public GameObject reticleObj;
    private Image reticle;
    private readonly Color noHitColor = new Color(0, 0, 0, 0.6f);
    private readonly Color hitColor = new Color(1, 1, 1, 0.6f);
    private readonly Color grabColor = new Color(1, 1, 1, 1);

    public float hitDistance = 2;
    private bool hit;

    private int layerMask8 = 1 << 8;
    private int layerMask9 = 1 << 9;
    private int layerMask10 = 1 << 10;
    private int layerMask11 = 1 << 11;
    private int layerMask12 = 1 << 12;

    private int layerMaskAll;

	// Use this for initialization
	void Start () {
	    layerMaskAll = layerMask8 | layerMask9 | layerMask10 | layerMask11 | layerMask12;
	    reticle = reticleObj.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
	    hit = Physics.Raycast(transform.position, transform.forward, hitDistance, layerMaskAll);
	    reticle.color = hit ? hitColor : noHitColor;

	    if (Input.GetButton("Fire1")) {
	        reticle.color = grabColor;
	    }

	    if (!reticleObj.activeSelf) {
	        reticle.color = Color.clear;
	    }
	}   
}
