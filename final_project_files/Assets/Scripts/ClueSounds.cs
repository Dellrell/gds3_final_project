﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Clue Sounds", fileName = "clueSounds")]
public class ClueSounds : ScriptableObject {

    public AudioClip[] sounds;

    public AudioClip GetCurrentClip(int currentPuzzle) {
        if (sounds == null || sounds.Length <= 0 || currentPuzzle > sounds.Length - 1) {
            return null;
        }

        return sounds[currentPuzzle];
    }
}
