﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FurnitureDeactivator : MonoBehaviour {

    public bool delayDeactivation = true;
    private GameObject player;

    void Start() {
        player = GameObject.FindGameObjectWithTag("Player");
    }

	// Update is called once per frame
	void Update () {
	    if (!delayDeactivation) {
	        if (PuzzleTracker.Instance.starsSolved) {
                this.gameObject.SetActive(false);
	        }
	    }
	    else {
	        if (PuzzleTracker.Instance.starsSolved && player.transform.position.y > 8) {
                this.gameObject.SetActive(false);
	        }
	    }
	}
}
