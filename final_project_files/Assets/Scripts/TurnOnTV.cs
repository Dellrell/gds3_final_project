﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOnTV : MonoBehaviour {

    public GameObject offLight;
    public GameObject offScreen;
    public GameObject onScreen;

    private bool hitButton;
    private bool tvIsOn;
    private RaycastHit rayHit;
    private AudioSource buttonSound;
	
	// Update is called once per frame
	void Update () {
	    hitButton = Physics.Raycast(transform.position, transform.forward, out rayHit, 2, 1 << 11) &&
	                rayHit.transform.gameObject.tag == "Button";
	    if (hitButton && Input.GetButtonUp("Fire1")) {
	        buttonSound = rayHit.transform.gameObject.GetComponent<AudioSource>();
            buttonSound.Play();
	        if (!tvIsOn) {
                onScreen.SetActive(true);
                offScreen.SetActive(false);
                offLight.SetActive(false);
	            tvIsOn = true;
	        }
	        else {
	           	        
	            onScreen.SetActive(false);
	            offScreen.SetActive(true);
	            offLight.SetActive(true);
	            tvIsOn = false; 
	        }
	    }
	}
}
