﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleTracker : MonoBehaviour {

    private static PuzzleTracker _instance;
    public static PuzzleTracker Instance { get { return _instance; } }

    public int puzzlesCompleted;
    public int level;
    public bool lookingThroughTelescope;
    public bool constellationViewed;

    void Awake() {
        if (_instance != null && _instance != this) {
            Destroy(this);
        }
        else {
            _instance = this;
        }

        puzzlesCompleted = 0;
        level = 1;
    }

    public GameObject globePiece1;
    public GameObject globePiece2;
    public GameObject globePiece3;
    public GameObject globePiece4;

    private float globePiece1Rot;
    private float globePiece2Rot;
    private float globePiece3Rot;
    private float globePiece4Rot;


    //piece 1
    private float[] piece1Mins = new float[] {-19, -119, -26, 120, -130};
    private float[] piece1Maxs = new float[] {22, -93, 0, 150, -99};
    //piece 2
    private float[] piece2Mins = new float[] {-43, -113, -140, 95, 100};
    private float[] piece2Maxs = new float[] {2, -80, -125, 125, 122};
    //piece 3
    private float[] piece3Mins = new float[] {25, 45, 30, 33, -14};
    private float[] piece3Maxs = new float[] {60, 75, 55, 57, 8};
    //piece 4
    private float[] piece4Mins = new float[] {-25, -114, -105, -115, 115};
    private float[] piece4Maxs = new float[] {3, -87, -70, -84, 132};

    private bool[] puzzlesSolved = new bool[] {false, false, false, false, false};

    //objects for transition to piano
    public GameObject piano;
    public GameObject room;
    public GameObject skybox;
    public GameObject mainLightObj;
    private Light mainLight;

    public bool starsSolved;

    public ClueSounds clueSounds;
    private AudioSource clueAudioSource;
    private float timer;

    public GameObject stereo;
    public GameObject lidAudio;
    public GameObject bgmusicPlayer;
    public GameObject player;
    private bool playerOutside;

    void Start() {
        mainLight = mainLightObj.GetComponent<Light>();
        clueAudioSource = GetComponent<AudioSource>();
    }
    

    void Update () {
        //get the y rotation that actually shows in the editor, otherwise as you rotate, the angle just keeps going up and up 
	    globePiece1Rot = Mathf.DeltaAngle(0, globePiece1.transform.eulerAngles.y);
	    globePiece2Rot = Mathf.DeltaAngle(0, globePiece2.transform.eulerAngles.y);
	    globePiece3Rot = Mathf.DeltaAngle(0, globePiece3.transform.eulerAngles.y);
	    globePiece4Rot = Mathf.DeltaAngle(0, globePiece4.transform.eulerAngles.y);

        if (lookingThroughTelescope) {
            if (puzzlesCompleted < 5) {
                CheckPuzzles();
            }
            else {
                room.SetActive(false);
                skybox.SetActive(false);
                piano.SetActive(true);
                starsSolved = true;
                mainLight.intensity = 1;
            }
        }

        clueAudioSource.clip = clueSounds.GetCurrentClip(puzzlesCompleted);
        timer += Time.deltaTime;
        if (clueAudioSource.clip != null && !starsSolved) {
            if (timer > 10) {
                PlayClue();
            }
        }

        if (starsSolved && !playerOutside) {
            stereo.SetActive(false);
            lidAudio.SetActive(true);
        }

        if (player.transform.position.y > 8) {
            lidAudio.SetActive(false);
            playerOutside = true;
            bgmusicPlayer.SetActive(true);
        }

    }//end update

    void CheckPuzzles() {
        //Debug.Log("checking puzzles");
        int activePuzzle = puzzlesCompleted;

            if (
                (globePiece1Rot >= piece1Mins[activePuzzle] && globePiece1Rot <= piece1Maxs[activePuzzle]) 
                && (globePiece2Rot >= piece2Mins[activePuzzle] && globePiece2Rot <= piece2Maxs[activePuzzle])
                && (globePiece3Rot >= piece3Mins[activePuzzle] && globePiece3Rot <= piece3Maxs[activePuzzle])
                && (globePiece4Rot >= piece4Mins[activePuzzle] && globePiece4Rot <= piece4Maxs[activePuzzle])
            ) {
                puzzlesSolved[activePuzzle] = true;
                Debug.Log(puzzlesSolved[activePuzzle]);
                puzzlesCompleted++;
                Debug.Log(puzzlesCompleted);
                constellationViewed = false; 
            }//end if
    }

    void PlayClue() {
        clueAudioSource.Play();    
        timer = 0;
    }
}
