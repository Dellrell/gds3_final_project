﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LookThroughTelescope : MonoBehaviour {

    public GameObject telescopeVignette;
    public GameObject reticle;
    public GameObject telescopeCameraObj;

    public StarDisplay starDisplay;
    public GameObject starsPanel;
    private Image starSprite;

    private Camera mainCamera;

    private float hitDistance = 2;
    private int layerMask = 1 << 10;
    private bool telescopeHit;
    public bool lookingThroughTelescope;

    public GameObject player;
    private MonoBehaviour mouseLookPlayer;
    private MonoBehaviour mouseLookCamera;
    private MonoBehaviour playerMovement;

	// Use this for initialization
	void Start () {
	    mouseLookPlayer = player.GetComponent<MouseLook>();
	    mouseLookCamera = GetComponent<MouseLook>();
	    playerMovement = player.GetComponent<PlayerMovement>();
        starSprite = starsPanel.GetComponent<Image>(); 
	}
	
	// Update is called once per frame
	void Update () {
	    telescopeHit = Physics.Raycast(transform.position, transform.forward, hitDistance, layerMask) && !lookingThroughTelescope;
        starSprite.sprite = starDisplay.GetCurrentSprite(PuzzleTracker.Instance.puzzlesCompleted - 1);

	    if (telescopeHit && Input.GetButtonUp("Fire1")) {
	        lookingThroughTelescope = true;
	    } else if (lookingThroughTelescope && Input.GetButtonUp("Fire1") && !starDisplay.inCutScene) {
	        lookingThroughTelescope = false;
            if(PuzzleTracker.Instance.puzzlesCompleted > 0 && !PuzzleTracker.Instance.constellationViewed) {
                PuzzleTracker.Instance.constellationViewed = true;
            }
	    }
	        

	    if (lookingThroughTelescope) {
            telescopeCameraObj.SetActive(true);
            telescopeVignette.SetActive(true);
            reticle.SetActive(false);
	        mouseLookPlayer.enabled = false;
	        mouseLookCamera.enabled = false;
	        playerMovement.enabled = false;
	        PuzzleTracker.Instance.lookingThroughTelescope = true;
            if (PuzzleTracker.Instance.puzzlesCompleted > 0 && !PuzzleTracker.Instance.constellationViewed) {
                starsPanel.SetActive(true);
                StartCoroutine(starDisplay.FadeInStars(starSprite));
                //PuzzleTracker.Instance.constellationViewed = true;
            } else {
                starsPanel.SetActive(false);
            }
	    }
	    else if (!InspectionMode.inInspectionMode) {
	        telescopeCameraObj.SetActive(false);
	        telescopeVignette.SetActive(false);
	        reticle.SetActive(true);
            starsPanel.SetActive(false);
	        mouseLookPlayer.enabled = true;
	        mouseLookCamera.enabled = true;
	        playerMovement.enabled = true;
	        PuzzleTracker.Instance.lookingThroughTelescope = false;
	    }
	}
}
