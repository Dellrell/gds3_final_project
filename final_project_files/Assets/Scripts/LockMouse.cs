﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockMouse : MonoBehaviour {

    public GameObject cursor;
    public GameObject reticle;
    private int layerMask8 = 1 << 8;
    private int layerMask9 = 1 << 9;
    private int layerMask12 = 1 << 12;
    private int unpausedMask;
    private int pausedMask = 1 << 5;

    void Start() {
        unpausedMask = layerMask8 | layerMask9 | layerMask12;
    }

	void Update () {
	    if (!InspectionMode.inInspectionMode && !MenuManager.gameIsPaused) {
	        Cursor.lockState = CursorLockMode.Locked;
	        Cursor.visible = false;
            InspectionMode.DisplayCursor(cursor, reticle, unpausedMask);
	    } else if (Input.GetKey(KeyCode.Escape) || InspectionMode.inInspectionMode) {
	        Cursor.lockState = CursorLockMode.None;
	        Cursor.visible = false;
            InspectionMode.DisplayCursor(cursor, reticle, unpausedMask);
	    } else if (MenuManager.gameIsPaused) {
	        Cursor.lockState = CursorLockMode.None;
	        Cursor.visible = false;
            InspectionMode.DisplayCursor(cursor, reticle, pausedMask);
	    }
	}
}
