﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float speed = 5.0f;
    public float gravity = 10.0f;
    public float jumpSpeed = 6.0f;
    private float horizontalInput;
    private float verticalInput;
    private int jumpTimer;
    private int jumpReset = 1;
    private CharacterController controller;
    private Vector3 movement = Vector3.zero;


	// Use this for initialization
	void Start () {
	    controller = GetComponent<CharacterController>();
	}
	
	void FixedUpdate () {
        //get input
	    horizontalInput = Input.GetAxis("Horizontal");
	    verticalInput = Input.GetAxis("Vertical");

	    if (controller.isGrounded) {
            //apply input
	        movement = new Vector3(horizontalInput, 0, verticalInput);
            movement = transform.TransformDirection(movement) * speed;

            if (!Input.GetButton("Jump")) {
	            jumpTimer++;
	        } else if (jumpTimer >= jumpReset) {
	            movement.y = jumpSpeed;
	            jumpTimer = 0;
	        }

	    }

	    //apply gravity
	    movement.y -= gravity * Time.deltaTime;
	    //move controller
	    controller.Move(movement * Time.deltaTime);
    }
}
