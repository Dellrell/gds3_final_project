﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Input = UnityEngine.Input;


[RequireComponent(typeof(Rigidbody))]

public class PickupableObject : MonoBehaviour {

    private Transform mainCam;

    void Start() {
        mainCam = Camera.main.transform;
    }

    void OnMouseDrag() {
        //Debug.Log("pickupable drag");
        if (InspectionMode.inInspectionMode) {
            InspectionMode.InspectObject(transform, mainCam);
        }
    }

}
