﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractWithEnvironment : MonoBehaviour {

    public GameObject player;
    private MonoBehaviour mouseLookPlayer;
    private MonoBehaviour mouseLookCamera;
    private MonoBehaviour playerMovement;

    public float hitDistance = 2;
    private int layerMask8 = 1 << 8;
    private int layerMask9 = 1 << 9;
    private int layerMask12 = 1 << 12;
    private int layerMaskInteractable;
    private bool pickupableHit;
    private bool interactableHit;
    private bool holdingObject;

    private GameObject hitObj;
    private Rigidbody hitRigid;
    private RaycastHit rayHit1;
    private RaycastHit rayHit2;

    private Vector3 lastGrabbedPosition;
    private Vector3 throwVector;
    private float throwForce;
    public float throwForceModFactor = 0.2f;
    private Vector3 throwVelocity;

	// Use this for initialization
	void Start () {
	    layerMaskInteractable = layerMask9 | layerMask12;
	    mouseLookPlayer = player.GetComponent<MouseLook>();
	    mouseLookCamera = GetComponent<MouseLook>();
	    playerMovement = player.GetComponent<PlayerMovement>();
	}
	
	// Update is called once per frame
	void Update () {

	    //pickup raycast
	    pickupableHit = Physics.Raycast(transform.position, transform.forward, out rayHit1, hitDistance, layerMask8);
	    Debug.DrawRay(transform.position, transform.forward * hitDistance, Color.red);
	    //environmet manipulator raycast
	    interactableHit = Physics.Raycast(transform.position, transform.forward, out rayHit2, hitDistance, layerMaskInteractable);
        
	    if (pickupableHit) {
	        hitObj = rayHit1.transform.gameObject;
	        hitRigid = hitObj.GetComponent<Rigidbody>();
	        if (Input.GetButton("Fire1") && !InspectionMode.inInspectionMode) {
	            holdingObject = true;
	        }
	    } else if (!InspectionMode.inInspectionMode) {
	        holdingObject = false;
	    }

	    if (Input.GetButtonUp("Fire1") && holdingObject && !InspectionMode.inInspectionMode) {
	        holdingObject = false;
	        throwVector = hitObj.transform.position - lastGrabbedPosition;
	        throwForce = (throwVector.magnitude / Time.deltaTime) * throwForceModFactor;
	        throwVelocity = throwVector.normalized * throwForce;
	        hitRigid.velocity = throwVelocity;
	    }

	    if (holdingObject) {
	        hitObj.transform.SetParent(transform);
	        hitRigid.isKinematic = true;
	        lastGrabbedPosition = hitObj.transform.position;
	    }
	    else if (hitObj != null && hitRigid != null){
	        hitObj.transform.SetParent(null);
	        hitRigid.isKinematic = false;
	    }
                    
	    if (Input.GetKeyDown(KeyCode.F) && holdingObject) {
	        // enter/exit inspectionmode
	        float distance = Vector3.Distance(transform.position, hitObj.transform.position);
	        InspectionMode.ActivateInspectionMode(distance);
	    }

	    if (interactableHit) {
	        hitObj = rayHit2.transform.gameObject;
	        if (Input.GetKeyDown(KeyCode.F)) {
	            float distance = Vector3.Distance(transform.position, hitObj.transform.position);
	            InspectionMode.ActivateInspectionMode(distance);
	        }
	    } else if (InspectionMode.inInspectionMode && Input.GetKeyDown(KeyCode.F) && !pickupableHit) {
            InspectionMode.ActivateInspectionMode(2);
	    }

	    if (InspectionMode.inInspectionMode) {
	        mouseLookPlayer.enabled = false;
	        mouseLookCamera.enabled = false;
	        playerMovement.enabled = false;
	    }
	    else {
	        mouseLookPlayer.enabled = true;
	        mouseLookCamera.enabled = true;
	        playerMovement.enabled = true;
	    }
        
	}
}
