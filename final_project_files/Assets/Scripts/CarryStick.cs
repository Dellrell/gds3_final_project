﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarryStick : MonoBehaviour {

    public GameObject glasses;
    public GameObject guideStick;
    private MeshCollider stickCollider;

	// Use this for initialization
	void Start () {
	    stickCollider = guideStick.GetComponent<MeshCollider>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (PuzzleTracker.Instance.puzzlesCompleted == 4) {
	        glasses.SetActive(false);
	        stickCollider.enabled = false;
            guideStick.transform.SetParent(this.transform);
            guideStick.transform.localPosition = new Vector3(-0.314f, -0.422f, 0.466f);
            guideStick.transform.localRotation = Quaternion.Euler(193.43f, 261.25f, -202.062f);
	    }
	}
}
