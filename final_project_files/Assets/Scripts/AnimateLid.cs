﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateLid : MonoBehaviour {

    private Vector3 lidRot;
    private Vector3 openRot = new Vector3(0, 0, -43.884f);
	
	// Update is called once per frame
	void Update () {

        lidRot = transform.eulerAngles;
        if (PuzzleTracker.Instance.starsSolved && !PuzzleTracker.Instance.lookingThroughTelescope) {
            StartCoroutine(LerpLid());
        }
    }

    IEnumerator LerpLid() {
        yield return new WaitForSeconds(2);

        while (lidRot.z > openRot.z) {
            Quaternion targetRot = Quaternion.Euler(openRot);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, 0.05f * Time.deltaTime);
            yield return null;
        }
    }
}
