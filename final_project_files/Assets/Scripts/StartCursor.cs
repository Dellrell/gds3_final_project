﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCursor : MonoBehaviour {
    
    public Texture2D cursor;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
	
	// Update is called once per frame
	void Update () {
        Cursor.SetCursor(cursor, hotSpot, cursorMode);
	}
}
