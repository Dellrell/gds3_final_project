﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InspectionMode {
    public static float InspectionSensitivity = 20.0f;

    public static bool inInspectionMode;
    private static readonly Color noHitColor = new Color(0, 0, 0, 0.6f);
    private static readonly Color hitColor = new Color(1, 1, 1, 0.6f);
    private static readonly Color grabColor = new Color(1, 1, 1, 1);

    private static readonly Color menuColor = new Color(0.1f, 0.1f, 0.1f, 1);

    private static bool cursorHit;
    private static Ray ray;

    
    //TODO can't lerp camera fov here because this function is only called during the one frame when F is first pressed, find another place to lerp maybe
    public static void ActivateInspectionMode(float distanceToObj) {
        if (!inInspectionMode) {
            if (distanceToObj >= 2) {
                Camera.main.fieldOfView = 25; 
            } else if (distanceToObj < 2 && distanceToObj >= 1.5f) {
                Camera.main.fieldOfView = 35;  
            } else if (distanceToObj < 1.5f && distanceToObj >= 1) {
                Camera.main.fieldOfView = 45;  
            } else if (distanceToObj < 1) {
                Camera.main.fieldOfView = 55; 
            }
            inInspectionMode = true;
        }
        else {
            Camera.main.fieldOfView = 60; 
            inInspectionMode = false;
        }
    }

    public static void DisplayCursor(GameObject cursor, GameObject reticle, LayerMask layerMask) {
        if (inInspectionMode) {
            cursor.SetActive(true);
            reticle.SetActive(false);
            cursor.transform.position = Input.mousePosition;
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            cursorHit = Physics.Raycast(ray, 2, layerMask);
            cursor.GetComponent<Image>().color = cursorHit ? hitColor : noHitColor;
            if (cursorHit && Input.GetButton("Fire1"))
                cursor.GetComponent<Image>().color = grabColor;
        } else if (MenuManager.gameIsPaused) {
            cursor.SetActive(true);
            reticle.SetActive(false);
            cursor.transform.position = Input.mousePosition;
            cursor.GetComponent<Image>().color = menuColor;
        }
        else {
            cursor.SetActive(false);
            reticle.SetActive(true);
        }   
    }
    
    public static void InspectObject(Transform pickupTrans, Transform camTransform) {
        float hRotation = 0;
        float vRotation = 0;
        hRotation += Input.GetAxis("Mouse X") * InspectionSensitivity;
        vRotation += Input.GetAxis("Mouse Y") * InspectionSensitivity;
        pickupTrans.Rotate(camTransform.up, -hRotation, Space.World); 
        pickupTrans.Rotate(camTransform.right, vRotation, Space.World);
    }

    public static void TwistGlobe(Transform globeTrans) {
        float hRotation = 0;
        hRotation += Input.GetAxis("Mouse X") * InspectionSensitivity;
        globeTrans.Rotate(Vector3.up, -hRotation, Space.Self);
    }

    public static void SlideFrame(Transform frameTrans) {
        frameTrans.Translate(0, 0, Input.GetAxis("Mouse X") * Time.deltaTime);

        /*
        Vector3 pos = frameTrans.localPosition;
        pos += new Vector3(0, 0, Input.GetAxis("Mouse X") * -0.01f);
        frameTrans.Translate(pos);
        pos.z = Mathf.Clamp(pos.z, 15.079f, 14.3f);
        */
    }

}
