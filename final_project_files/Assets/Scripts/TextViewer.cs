﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextViewer : MonoBehaviour {
    public GameObject instructionTextObj;
    public GameObject flavourTextObj;

    private Text instructionText;
    private Outline instructionOutline;

    private Text flavourText;
    private Outline flavourOutline;

    public Color textColor = new Color(1, 1, 1, 1);
    public Color outlineColor = new Color(0.08f, 0f, 0.25f, 1);
    private LookThroughTelescope scope;

    public float hitDistance = 2;
    private bool hitFlavour;
    private bool hitF;
    private bool hitClickable;
    private bool hitPickupable;
    private RaycastHit rayHitFlavour;

    private int layerMask8 = 1 << 8;
    private int layerMask9 = 1 << 9;
    private int layerMask10 = 1 << 10;
    private int layerMask11 = 1 << 11;
    private int layerMask12 = 1 << 12;

    private int layerMaskAll;
    private int layerMaskF;
    private int layerMaskClickable;
    private int layerMaskPickupable;

	// Use this for initialization
	void Start () {
	    layerMaskAll = layerMask8 | layerMask9 | layerMask10 | layerMask11 | layerMask12;
	    layerMaskF = layerMask9 | layerMask12;
	    layerMaskClickable = layerMask10;
	    layerMaskPickupable = layerMask8;

	    instructionText = instructionTextObj.GetComponent<Text>();
	    instructionOutline = instructionTextObj.GetComponent<Outline>();

	    flavourText = flavourTextObj.GetComponent<Text>();
	    flavourOutline = flavourTextObj.GetComponent<Outline>();

	    instructionOutline.effectColor = outlineColor;
	    flavourOutline.effectColor = outlineColor;

	    instructionText.color = Color.clear;
	    flavourText.color = Color.clear;

	    scope = GetComponent<LookThroughTelescope>();

	}
	
	// Update is called once per frame
	void Update () {
	    hitFlavour = Physics.Raycast(transform.position, transform.forward, out rayHitFlavour, hitDistance, layerMaskAll);
	    hitF = Physics.Raycast(transform.position, transform.forward, hitDistance, layerMaskF);
	    hitClickable = Physics.Raycast(transform.position, transform.forward, hitDistance, layerMaskClickable);
        hitPickupable = Physics.Raycast(transform.position, transform.forward, hitDistance, layerMaskPickupable);

	    if (hitFlavour && !InspectionMode.inInspectionMode && !scope.lookingThroughTelescope) {
	    GameObject viewedObject = rayHitFlavour.transform.gameObject;
	    Description viewedDescription = viewedObject.GetComponent<Description>();

	        if (viewedDescription != null) {
	            flavourText.text = viewedDescription.objectDescription;
	            flavourText.color = Color.Lerp(flavourText.color, textColor, 0.05f);
	        }
	    } 
	    else {
	        flavourText.color = Color.Lerp(flavourText.color, Color.clear, 0.05f);
	    }

	    if (hitF) {
	        if (!InspectionMode.inInspectionMode) {
	            instructionText.text = "Press F to Inspect";
	            instructionText.color = Color.Lerp(instructionText.color, textColor, 0.05f);
	        } else if (InspectionMode.inInspectionMode) {
	            instructionText.text = "Click and Drag to Interact";
	            instructionText.color = Color.Lerp(instructionText.color, textColor, 0.05f);
	        }
	    }
	    else if (hitClickable && !scope.lookingThroughTelescope) {
	        instructionText.text = "Click to Interact";
	        instructionText.color = Color.Lerp(instructionText.color, textColor, 0.05f);
	    }
	    else if (hitPickupable) {
	        if (!InspectionMode.inInspectionMode && !Input.GetButton("Fire1")) {
	            instructionText.text = "Click to Grab";
	            instructionText.color = Color.Lerp(instructionText.color, textColor, 0.05f);
	        } else if (!InspectionMode.inInspectionMode && Input.GetButton("Fire1")) {
	            instructionText.text = "Press F to Inspect";
	            instructionText.color = Color.Lerp(instructionText.color, textColor, 0.05f);
	        } else if (InspectionMode.inInspectionMode) {
	            instructionText.text = "Click and Drag to Interact";
	            instructionText.color = Color.Lerp(instructionText.color, textColor, 0.05f);
	        }
	    }
	    else {
	        instructionText.color = Color.Lerp(instructionText.color, Color.clear, 0.05f);
	    }
	}
}
