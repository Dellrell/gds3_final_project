﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentManipulator : MonoBehaviour{

    void OnMouseDrag() {
        //Debug.Log("pickupable drag");
        if (InspectionMode.inInspectionMode) {
            InspectionMode.TwistGlobe(transform);
        }
    }
}
